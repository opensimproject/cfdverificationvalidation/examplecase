
# Example test case

The aim of this example repository is to provide you with a template when developing a new verification or validation case. We suggest that you start by providing a brief description of the test case and what you want to verify or validate.

## Admin

As few enjoy the administrative component of life, we try to keep it to a minimal. There are, however, some important points that need to be captured.

### Type of problem

Please state if it is a verification or validation case. If it is a verification case the results should at least be compared to an analytical solution and in the case of the later to experimental data. Please recognise and cite the source of the data.

### Software version

Please list the software and version here:
Eg. OpenFOAM v2006.

If you use any Python scripts please provide the version as well as the *requirements.txt* file with the library dependency.

### Contributors

It is important for us that those give their time and effort to better the community should receive the recognition. Although your contribution is visible in the commit history, we ask that the original contributor and any major contributions also be listed in the table.
 
| Name | Orginasation | Contribution |
|-|-|-|
|John Cleese   | Life of modelling Inc | Original contributor |
|Michael Palin | Holy grail of simulation Inc | Port to version 5.3 |

### Some extra notes

Where possible stick to the standard structure that the community is used too. It makes it easier for everyone. For example, in the case of OF, please include the *0.orig*, *constant* and *system* folders as well as *clean* and *run* bash scripts. 

## Problem description 

Please provide a more detailed description here and a schematic representation will be greatly appreciated.

## Scripts

A good engineer is a lazy one. For this reason, we expect that each V&V case will have several accompanying scripts to automate the workflow. Please list the relevant of scripts provided for the user below.

* *clean.sh* - Removes the files generated during the run.
* *run.sh* - Create the mesh, run setFields utility and the electric solver.

## Mesh

All computational models require a mesh and it is often the most tedious and cumbersome part of the process. If the mesh can be generated through open-source software (eg. GMSH or snappyHexMesh) through an input text file, please provide these instead of the generated mesh file.  If, however, you had to create the mesh with proprietary software (which not available to the broader community) please include the mesh file in the repository. 
 
If possible it is possible, it would be nice to see both a nice a well behaved structured mesh as well as a nasty unstructured mesh. Let us be honest it is the latter that we see more often and which causes the most nightmares in terms of numerical stability and accuracy.

## Boundary and initial conditions

Everything needs to start somewhere.

## Results

Some pretty pictures, it is after all it is Colorful Aero Dynamics!


